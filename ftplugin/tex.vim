" Detect correct filetype everywhere
let g:tex_flavor = "latex"

set spell spelllang=de_de

" Folding
let g:tex_fold_enabled=1
set foldmethod=syntax

" Overwrite standard indentation width
" 4 provides correct indentation for markdown lists
set tabstop=4
set shiftwidth=4
