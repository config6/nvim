""""""""""""""""""""
" control settings "
""""""""""""""""""""

" plugin control settings
source ~/.config/nvim/controls-coc.vim


" Behave like a desktop editor/ide
set mouse=a

" Map the leader key to SPACE
let mapleader="\<SPACE>"



" Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
endif


" Search and Replace
nmap <Leader>s :%s//g<Left><Left>
" Call make
nmap <Leader>m :!make<CR>
" Grammar check
nmap <Leader>g :LanguageToolCheck<CR>
" Toggle Folding
nmap <Leader><Leader> za
" Shortcut to use the vim-websearch extension
nmap <Leader>ws :WebSearch<Space>
" Spotify play/pause
nmap <Leader>pp :Spotify<CR>
" Tablular toggle
nmap <Leader>tm :TableModeToggle
" Switch windows
map <Leader><Right> <C-W><Right>
map <Leader><Left> <C-W><Left>
map <Leader><Up> <C-W><Up>
map <Leader><Down> <C-W><Down>
