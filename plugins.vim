" Install vim-plug if not done yet
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
        silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
                                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


" Install nodejs for COC
if empty(glob('~/.nodejs/bin/node'))
        !mkdir ~/.nodejs
        !curl -sL install-node.now.sh/lts | sh -s -- --prefix=$HOME/.nodejs --yes | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g"
endif


" Install yarn for COC
if empty(glob('~/.yarn/bin/yarn')) && empty(glob('/bin/yarn'))
        !curl -o- -L https://yarnpkg.com/install.sh | bash | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g"
endif


" List of Plugins for vim-plug
call plug#begin('~/.local/share/nvim/plugged')
Plug 'sainnhe/vim-color-forest-night'  " Theme
Plug 'neoclide/coc.nvim', {'branch': 'release'}  " Auto complete
Plug 'lervag/vimtex'
Plug 'honza/vim-snippets'
Plug 'dpelle/vim-LanguageTool'  " Grammar
Plug 'HendrikPetertje/vimify'  " Spotify
Plug 'linluk/vim-websearch'
Plug 'dhruvasagar/vim-table-mode'
Plug 'frankier/neovim-colors-solarized-truecolor-only'
call plug#end()


""""""""""
" Config "
""""""""""

" coc
" Add nodejs to path
"let g:coc_node_path = $HOME
let g:coc_node_path = $HOME . '/.nodejs/bin/node'
" Add yarn to path
let $PATH .= ':' . $HOME . '/.yarn/bin'

" extensions
let g:coc_global_extensions = [
                        \ 'coc-snippets',
                        \ 'coc-python',
                        \ 'coc-texlab'
                        \ ]
" TODO: automatic pip install jedi for coc-python


" vim-LanguageTool
let g:languagetool_jar='$HOME/.LanguageTool/languagetool-commandline.jar'
" let g:languagetool_lang='de-DE'


" vim-websearch
let g:web_search_command = "firefox"


" vimtex, add -shell-escape and -pdf option to latexmk
let g:vimtex_compiler_latexmk = {
                        \ 'backend' : 'nvim',
                        \ 'background' : 1,
                        \ 'build_dir' : '',
                        \ 'callback' : 1,
                        \ 'continuous' : 1,
                        \ 'executable' : 'latexmk',
                        \ 'hooks' : [],
                        \ 'options' : [
                        \   '-pdf',
                        \   '-shell-escape',
                        \   '-verbose',
                        \   '-file-line-error',
                        \   '-synctex=1',
                        \   '-interaction=nonstopmode',
                        \ ],
                        \}
