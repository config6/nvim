""""""""""""""""""
" basic settings "
""""""""""""""""""


" numbering
set number
set relativenumber
" switch to normal numbers in : mode
autocmd CmdlineEnter * set norelativenumber | redraw!
autocmd CmdlineLeave * set relativenumber | redraw!


" central undofiles
set undofile
set undodir=~/.config/nvim/undodir/


" backup a file before editing it
set backup
set backupdir=~/.config/nvim/backupdir/


" cursor jumps to matching brackets for a short time
set showmatch
set matchtime=5


" settings for indentation
set expandtab " insert spaces instead of tab
set tabstop=8 " size of a tab
set shiftwidth=8 " < > width


" splitting windows
set splitbelow
set splitright


" search
set ignorecase          " Make searching case insensitive
set smartcase           " ... unless the query has capital letters.


" jump to last editing position
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


" Detect correct filetype everywhere
let g:tex_flavor = "latex"
