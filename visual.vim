"""""""""""""""""""
" visual settings "
"""""""""""""""""""

" Set color scheme to forest-night
syntax enable
set background=light
set termguicolors
colorscheme solarized

" Highlight all tabs and trailing whitespace characters
highlight ExtraWhitespace ctermbg=red guibg=darkgreen
match ExtraWhitespace /\s\+$\|\t/

if &diff
   highlight DiffAdd    cterm=bold ctermfg=8 ctermbg=47 gui=bold guifg=bg guibg=lightgreen
   highlight DiffDelete cterm=bold ctermfg=8 ctermbg=167 gui=bold guifg=bg guibg=lightred
   highlight DiffChange cterm=bold ctermfg=8 ctermbg=192 gui=bold guifg=bg guibg=lightyellow
   highlight DiffText   cterm=bold ctermfg=192 ctermbg=52 gui=bold guifg=fg guibg=darkred
endif
